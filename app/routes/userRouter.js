const express = require("express");

const userRouter = express.Router();

userRouter.get("/userRouter", (req, res) => {
    res.status(200).json({
        message: `GET ALL userRouter`
    })
})

userRouter.get("/userRouter/:userId",(req,res) =>{
    let userId = req.params.userId ;
    res.status(200).json({
        message: `GET userRouter BY ID : ` + userId 
    })
})

userRouter.post("/userRouter", (req, res) => {
    res.status(200).json({
        message: `CREATE userRouter`
    })
})

userRouter.put("/userRouter/:userId",(req,res) =>{
    let userId = req.params.userId ;
    res.status(200).json({
        message: `UPDATE userRouter WITH ID : ` + userId 
    })
})

userRouter.delete("/userRouter/:userId",(req,res) =>{
    let userId = req.params.userId ;
    res.status(200).json({
        message: `DELETE userRouter WITH ID : ` + userId 
    })
})

module.exports = userRouter;