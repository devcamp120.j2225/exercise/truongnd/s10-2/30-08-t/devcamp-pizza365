const express = require("express");

const voucherRouter = express.Router();

voucherRouter.get("/drinkVoucher", (req, res) => {
    res.status(200).json({
        message: `GET ALL drinkVoucher`
    })
})

voucherRouter.get("/drinkVoucher/:drinkVoucherId",(req,res) =>{
    let drinkVoucherId = req.params.drinkVoucherId ;
    res.status(200).json({
        message: `GET drinkVoucher BY ID : ` + drinkVoucherId 
    })
})

voucherRouter.post("/drinkVoucher", (req, res) => {
    res.status(200).json({
        message: `CREATE drinkVoucher`
    })
})

voucherRouter.put("/drinkVoucher/:drinkVoucherId",(req,res) =>{
    let drinkVoucherId = req.params.drinkVoucherId ;
    res.status(200).json({
        message: `UPDATE drinkVoucher WITH ID : ` + drinkVoucherId 
    })
})

voucherRouter.delete("/drinkVoucher/:drinkVoucherId",(req,res) =>{
    let drinkVoucherId = req.params.drinkVoucherId ;
    res.status(200).json({
        message: `DELETE drinkVoucher WITH ID : ` + drinkVoucherId 
    })
})

module.exports = voucherRouter;