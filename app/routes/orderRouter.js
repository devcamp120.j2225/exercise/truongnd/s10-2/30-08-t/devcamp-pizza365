const express = require("express");

const orderRouter = express.Router();

orderRouter.get("/orderRouter", (req, res) => {
    res.status(200).json({
        message: `GET ALL orderRouter`
    })
})

orderRouter.get("/orderRouter/:orderId",(req,res) =>{
    let orderId = req.params.orderId ;
    res.status(200).json({
        message: `GET orderRouter BY ID : ` + orderId 
    })
})

orderRouter.post("/orderRouter", (req, res) => {
    res.status(200).json({
        message: `CREATE orderRouter`
    })
})

orderRouter.put("/orderRouter/:orderId",(req,res) =>{
    let orderId = req.params.orderId ;
    res.status(200).json({
        message: `UPDATE orderRouter WITH ID : ` + orderId 
    })
})

orderRouter.delete("/orderRouter/:userId",(req,res) =>{
    let orderId = req.params.orderId ;
    res.status(200).json({
        message: `DELETE orderRouter WITH ID : ` + orderId 
    })
})

module.exports = orderRouter;