const express = require("express");

const drinkRouter = express.Router();

drinkRouter.get("/drinkList", (req, res) => {
    res.status(200).json({
        message: `GET ALL DRINK`
    })
})

drinkRouter.get("/drinkList/:drinkId",(req,res) =>{
    let drinkId = req.params.drinkId ;
    res.status(200).json({
        message: `GET DRINK BY ID : ` + drinkId 
    })
})

drinkRouter.post("/drinkList", (req, res) => {
    res.status(200).json({
        message: `CREATE DRINK`
    })
})

drinkRouter.put("/drinkList/:drinkId",(req,res) =>{
    let drinkId = req.params.drinkId ;
    res.status(200).json({
        message: `UPDATE DRINK WITH ID : ` + drinkId 
    })
})

drinkRouter.delete("/drinkList/:drinkId",(req,res) =>{
    let drinkId = req.params.drinkId ;
    res.status(200).json({
        message: `DELETE DRINK WITH ID : ` + drinkId 
    })
})

module.exports = drinkRouter;